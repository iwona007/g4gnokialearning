package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Streams {
    public static void run(){
        System.out.println("Czesc");

        List<Integer> lista = new ArrayList<>();
        lista.add(1);
        lista.add(2);
        lista.add(3);
        lista.add(4);

        Object numberOfElements = lista.stream()
                .count();

        Object numbersHigherThanTwo = lista.stream()
                .filter(liczba -> liczba>2)
                .collect(Collectors.toList());

        System.out.println(lista);
        System.out.println(numberOfElements);
        System.out.println(numbersHigherThanTwo);
    }
}
