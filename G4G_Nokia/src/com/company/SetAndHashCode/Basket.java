package com.company.SetAndHashCode;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

public class Basket extends HashSet<Buyable> implements Set<Buyable> {
    @Override
    public boolean add(Buyable fruit) {
        return super.add(fruit);
    }

    @Override
    public Stream<Buyable> stream() {
        return super.stream();
    }
}
