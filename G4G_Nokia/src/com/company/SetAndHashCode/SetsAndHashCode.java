package com.company.SetAndHashCode;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetsAndHashCode {
    public static void run(){
        Set<Buyable> newBasket = new TreeSet();
        Fruit jonagold = new Fruit(2.0, "jonagold");
        Fruit antonowka = new Fruit(2.0, "antonowke");
        Vegetable marchew = new Vegetable(2.0, "marchew");

        newBasket.add(jonagold);
        newBasket.add(antonowka);
        newBasket.add(marchew);

        System.out.println("Number of things in basket: " + newBasket.stream().count());
        newBasket.stream()
                .forEach( apple -> System.out.println(apple));

        Set<Buyable> newBasket2 = new TreeSet();
        newBasket2.stream()
                .forEach( apple -> System.out.println(apple));
    }
}
