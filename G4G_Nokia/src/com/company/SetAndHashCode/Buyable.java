package com.company.SetAndHashCode;

public class Buyable implements Comparable<Buyable>{
    private double energy;
    private String name;

    public Buyable(double energy, String name) {

        this.energy = energy;
        this.name = name;
    }

    public double getEnergy() {
        return energy;
    }

    public void setEnergy(double energy) {
        this.energy = energy;
    }

    public String toString(){
        return name + ' ' + energy;
    }

    @Override
    public int compareTo(Buyable o) {
        return (int)(this.energy-o.energy);
    }
}
